package file.operations;

import java.io.*;

/**
 * Created by PARASURAMAN on 8/28/2016.
 */
public class CreatingSimpleFile {
    public static void main(String[] args) {

        Thread[] threadArray = new Thread[100];

        for (int i=0;i<100;i++) {
            Thread thread = new Thread(new Job("Thread_number" + i));
            threadArray[i] = thread;
        }

        for (int i = 0; i < 100; i++) {
            threadArray[i].start();
        }
    }
}


class Job implements Runnable {

    private String creatingThreadName;

    public Job(String creatingThreadName) {
        this.creatingThreadName = creatingThreadName;
    }

    @Override
    public void run() {
        File file = new File("C:\\FileOperations\\directory\\"+creatingThreadName+".txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("IO exception");
        }

        if (creatingThreadName.equals("Thread_number25")) {
            file.delete();
            return;
        }

        try (PrintWriter printWriter = new PrintWriter(new FileOutputStream(file))) {
            printWriter.write(creatingThreadName+" has created this file ");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}