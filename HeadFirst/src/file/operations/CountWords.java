package file.operations;

import java.io.*;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by PARASURAMAN on 9/10/2016.
 */
public class CountWords {
    public static void main(String[] args) {
        File file1 = new File("D:\\source.txt");
        Map<String, Integer> map = new HashMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file1))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(" ");
                for (String s : split) {
                    if (map.containsKey(s)) {
                        Integer integer = map.get(s);
                        integer++;
                        map.put(s, integer);
                    } else {
                        map.put(s, 1);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file2 = new File("D:\\cl\\output.txt");


        try (PrintWriter printWriter = new PrintWriter(new FileOutputStream(file2))) {
            for (Map.Entry<String, Integer> stringIntegerEntry : map.entrySet()) {
                String s = stringIntegerEntry.getKey() + "  " + stringIntegerEntry.getValue();
                System.out.println(s);
                printWriter.append(s);
                printWriter.append("\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
