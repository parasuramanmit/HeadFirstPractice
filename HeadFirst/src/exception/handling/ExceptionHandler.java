package exception.handling;

/**
 * Created by PARASURAMAN on 8/28/2016.
 */
public class ExceptionHandler {
    public static void main(String[] args) throws Exception {
        int value = 9;
        int i = 1 << value;
        System.out.println(i);
        isUniqueChars("parasu");
    }

    public static boolean isUniqueChars(String str) {
         int checker = 0;
         for (int i = 0; i < str.length(); ++i) {
             int val = str.charAt(i) - 'a';
             if ((checker & (1 << val)) > 0) return false;
             checker |= (1 << val);
             }
         return true;
         }
}
