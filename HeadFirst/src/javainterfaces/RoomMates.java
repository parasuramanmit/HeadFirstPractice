package javainterfaces;

/**
 * Created by PARASURAMAN on 9/4/2016.
 */
public interface RoomMates {
    String getName();
    int getAge();
}
