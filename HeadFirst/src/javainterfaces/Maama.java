package javainterfaces;

/**
 * Created by PARASURAMAN on 9/4/2016.
 */
public class Maama implements RoomMates {
    @Override
    public String getName() {
        return "maama";
    }

    @Override
    public int getAge() {
        return 26;
    }
}
