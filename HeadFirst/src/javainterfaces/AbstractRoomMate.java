package javainterfaces;

import java.awt.*;

/**
 * Created by PARASURAMAN on 9/4/2016.
 */
public abstract class AbstractRoomMate {

    public String getGender() {
        return "male";
    }

    abstract String getName();

    abstract String getNative();
}

class NesaMani extends AbstractRoomMate{

    @Override
    String getName() {
        return "nesamani";
    }

    @Override
    String getNative() {
        return "namakal";
    }
}

class Amavasi extends AbstractRoomMate{

    @Override
    String getName() {
        return "amavasai";
    }

    @Override
    String getNative() {
        return "karur";
    }
}

