package ooad.practice.dogdoor;

/**
 * Created by PARASURAMAN on 5/8/2016.
 */
public class Bark {
    private String barkSound;

    public Bark(String barkSound) {
        this.barkSound = barkSound;
    }

    public String getBarkSound() {
        return barkSound;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bark bark = (Bark) o;

        return barkSound.equals(bark.barkSound);

    }

    @Override
    public int hashCode() {
        return barkSound.hashCode();
    }
}
