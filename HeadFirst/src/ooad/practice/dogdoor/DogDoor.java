package ooad.practice.dogdoor;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by PARASURAMAN on 5/8/2016.
 */
public class DogDoor {
    private boolean open;

    private List<Bark> allowedBarks;

    public DogDoor() {
        this.open = false;
    }

    public boolean isOpen() {
        return open;
    }

    public List<Bark> getAllowedBarks() {
        return allowedBarks;
    }

    public void setAllowedBarks(List<Bark> allowedBarks) {
        this.allowedBarks = allowedBarks;
    }

    public void open() {
        System.out.println("Opening the door");
        open = true;
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                close();
                timer.cancel();
            }
        },5000);
    }

    public void close() {
        System.out.println("Closing the door");
        open = false;
    }
}

