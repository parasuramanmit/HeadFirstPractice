package ooad.practice.dogdoor;

/**
 * Created by PARASURAMAN on 5/8/2016.
 */
public class BarkRecognizer {
    private DogDoor dogDoor;

    public BarkRecognizer(DogDoor dogDoor) {
        this.dogDoor = dogDoor;
    }

    public void recognize(Bark str) {
        if (dogDoor.getAllowedBarks().contains(str))
            dogDoor.open();
        System.out.println("Bark recognizer hears " + str);
    }
}
