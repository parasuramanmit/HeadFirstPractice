package ooad.practice.guitarapp;

/**
 * Created by parasuraman on 10/11/16.
 */
public abstract class Instrument {
    private String serialNUmber;
    private double price;
    private InstrumentSpec instrumentSpec;

    public Instrument(String serialNUmber, double price, InstrumentSpec instrumentSpec) {
        this.serialNUmber = serialNUmber;
        this.price = price;
        this.instrumentSpec = instrumentSpec;
    }

    public String getSerialNUmber() {
        return serialNUmber;
    }

    public double getPrice() {
        return price;
    }

    public InstrumentSpec getInstrumentSpec() {
        return instrumentSpec;
    }
}
