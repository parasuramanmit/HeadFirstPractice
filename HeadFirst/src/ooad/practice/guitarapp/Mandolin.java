package ooad.practice.guitarapp;

/**
 * Created by parasuraman on 10/11/16.
 */
public class Mandolin extends Instrument {

    private MandolinSpec mandolinSpec;

    public Mandolin(String serialNUmber , double price, MandolinSpec mandolinSpec) {
        super(serialNUmber, price, mandolinSpec);

        this.mandolinSpec = mandolinSpec;
    }

    public MandolinSpec getMandolinSpec() {
        return mandolinSpec;
    }
}
