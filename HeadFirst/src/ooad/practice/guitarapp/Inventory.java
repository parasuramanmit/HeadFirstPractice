package ooad.practice.guitarapp;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private List<Instrument> inventory;

    public Inventory() {
        inventory = new ArrayList<>();
    }

    public void addInstrument(String serialNumber, double price, InstrumentSpec instrumentSpec) {

        Instrument instrument = null;
        if (instrumentSpec instanceof GuitarSpec) {
            instrument = new Guitar(serialNumber, price, (GuitarSpec) instrumentSpec);
        } else if (instrumentSpec instanceof MandolinSpec) {
            instrument = new Mandolin(serialNumber, price, (MandolinSpec) instrumentSpec);
        }

        inventory.add(instrument);
    }

    public Instrument get(String serialNumber) {

        for (Instrument instrument : inventory) {
            if (instrument.getSerialNUmber().equals(serialNumber)) {
                return instrument;
            }
        }
        return null;
    }

    public List<Instrument> search(InstrumentSpec instrumentSpec) {

        List<Instrument> list = new ArrayList<>();
        for (Instrument instrument : inventory) {
            if (instrument.getInstrumentSpec().matches(instrumentSpec)) {
                list.add(instrument);
            }
        }

        return list;
    }


}
