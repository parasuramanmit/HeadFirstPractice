package ooad.practice.guitarapp;

/**
 * Created by parasuraman on 10/11/16.
 */
public abstract class InstrumentSpec {
    private Builder builder;
    private String model;
    private Type type;
    private Wood backWood;
    private Wood topWood;

    public InstrumentSpec(Builder builder, String model, Type type, Wood backWood, Wood topWood) {
        this.builder = builder;
        this.model = model;
        this.type = type;
        this.backWood = backWood;
        this.topWood = topWood;
    }

    public boolean matches(InstrumentSpec instrumentSpec) {

        if (builder != instrumentSpec.builder)
            return false;

        if (!(model.equals(instrumentSpec.model)))
            return false;

        if (type != instrumentSpec.type)
            return false;

        if (backWood != instrumentSpec.backWood)
            return false;

        if (topWood != instrumentSpec.topWood)
            return false;

        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InstrumentSpec that = (InstrumentSpec) o;

        if (builder != that.builder) return false;
        if (!model.equals(that.model)) return false;
        if (type != that.type) return false;
        if (backWood != that.backWood) return false;
        return topWood == that.topWood;

    }

    @Override
    public int hashCode() {
        int result = builder.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + backWood.hashCode();
        result = 31 * result + topWood.hashCode();
        return result;
    }
}
