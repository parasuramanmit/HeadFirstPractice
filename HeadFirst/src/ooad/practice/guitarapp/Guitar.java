package ooad.practice.guitarapp;

public class Guitar extends Instrument{

	private GuitarSpec guitarSpec;

	public Guitar(String serialnumber, double price, GuitarSpec guitarSpec) {
        super(serialnumber, price, guitarSpec);
	}

	public GuitarSpec getGuitarSpec() {
		return guitarSpec;
	}

}