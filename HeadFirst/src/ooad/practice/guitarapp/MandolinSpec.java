package ooad.practice.guitarapp;

/**
 * Created by parasuraman on 10/11/16.
 */
public class MandolinSpec extends InstrumentSpec{
    private Style style;

    public MandolinSpec(Builder builder, String model, Type type, Style style, Wood backWood, Wood topWood) {
        super(builder,model,type,backWood,topWood);
        this.style = style;
    }

    public Style getStyle() {
        return style;
    }

    @Override
    public boolean matches(InstrumentSpec instrumentSpec) {
        if (!super.matches(instrumentSpec)) {
            return false;
        }

        if (!(instrumentSpec instanceof MandolinSpec)) {
            return false;
        }

        MandolinSpec instrumentSpec1 = (MandolinSpec) instrumentSpec;
        if (style != instrumentSpec1.style) {
            return false;
        }
        return true;
    }
}
