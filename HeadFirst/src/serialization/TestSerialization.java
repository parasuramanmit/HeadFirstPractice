package serialization;

import java.io.*;

/**
 * Created by parasuraman on 14/9/16.
 */
public class TestSerialization {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fileOutputStream = new FileOutputStream("/tmp/OurObject.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        OurObject ourObject = new OurObject();
        objectOutputStream.writeObject(ourObject);
        objectOutputStream.close();
        fileOutputStream.close();


        FileInputStream fileInputStream = new FileInputStream("/tmp/Our Object.ser");
        ObjectInputStream is = new ObjectInputStream(fileInputStream);

        OurObject ourObject1 = (OurObject) is.readObject();
        System.out.println(ourObject1.anInt);
        is.close();
        fileInputStream.close();

    }

}


class OurObject implements Serializable {
    int anInt = 10;
    public final void writeObject(Object x) throws IOException{

    }
}
