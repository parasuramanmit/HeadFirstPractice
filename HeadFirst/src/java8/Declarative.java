package java8;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by PARASURAMAN on 8/27/2016.
 */
public class Declarative {

    private static int count = 0;

    private static boolean isGreaterThan3(int num) {
        System.out.println("Is greater than three " + num);
        count++;
        return num > 3;
    }

    private static boolean isEven(int num) {
        System.out.println("Is an even number "+num);
        count++;
        return num %2 == 0;
    }

    private static int doubleIt(int num) {
        System.out.println("Doubling the value for "+num);
        count++;
        return num * 2;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 3, 4, 5, 6, 7);


        list.stream().
                filter(Declarative :: isGreaterThan3).
                filter(Declarative::isEven).
                map(Declarative::doubleIt);


        // System.out.println(integer);
        System.out.println("Number of operations "+count);

    }
}
