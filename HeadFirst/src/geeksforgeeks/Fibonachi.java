package geeksforgeeks;

/**
 * Created by parasuraman on 11/1/17.
 */
public class Fibonachi {
    public static void main(String[] args) {
        wrirteFibonachiUpto(10);
        System.out.println();
        byRcursion(10);
    }

    private static void byRcursion(int i) {
        doTheRealWork(0, 1, i);
    }

    private static void doTheRealWork(int first, int second, int iterations) {

        iterations--;

        if(first == 0 && second ==1) {
            System.out.print(0 + " " + 1 + " ");
            iterations--;
        }

        int result = first + second;
        System.out.print(result+" ");

        if (iterations>0)
            doTheRealWork(second, result, iterations);


    }

    private static void wrirteFibonachiUpto(int i) {

        int first = 0;
        int second = 1;

        for (int j = 0; j < i; j++) {

            if (j == 0) {
                System.out.print(first+" ");continue;
            }

            if (j == 1) {
                System.out.print(second+" ");continue;
            }

            int result = first + second;
            System.out.print(result+" ");

            first = second;
            second = result;
        }
    }


}
