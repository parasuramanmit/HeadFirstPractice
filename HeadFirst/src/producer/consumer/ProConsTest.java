package producer.consumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ThreadFactory;

/**
 * Created by prakash on 8/11/2016.
 */
public class ProConsTest {

    private static Queue<Integer> queue =new LinkedList<Integer>();

    public static void main(String[] args) {

        Thread t1 = new Thread(new Producer(queue));
        t1.setName("Producer");
        Thread t2 = new Thread(new Consumer(queue));
        t2.setName("Consumer");
        t1.start();
        t2.start();
    }

}
