package producer.consumer;

import java.util.Queue;

/**
 * Created by prakash on 8/11/2016.
 */
public class Consumer implements Runnable {
    private Queue<Integer> queue;

    public Consumer(Queue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        synchronized (queue) {
            System.out.println("Consumer thread started");
            while (true) {
                while (queue.size() < 1) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Integer remove = queue.remove();
                System.out.println(Thread.currentThread() + " has received " + remove);
                queue.notify();
                if (remove == 5)
                    break;
            }
        }

    }
}
