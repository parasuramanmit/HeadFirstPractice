package producer.consumer;

import java.util.Queue;

/**
 * Created by prakash on 8/11/2016.
 */
public class Producer implements Runnable {

    private Queue<Integer> queue;

    public Producer(Queue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        synchronized (queue) {
            System.out.println("Producer thread started");
            int i = 0;
            while (true) {
                while (queue.size() >= 1) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread() + " is adding " + (++i) + " to the queue");
                queue.add(i);
                queue.notify();
                if (i > 4)
                    break;
            }
        }
    }
}
