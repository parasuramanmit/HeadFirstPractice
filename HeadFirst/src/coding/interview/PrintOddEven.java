package coding.interview;

/**
 * Created by parasuraman on 20/12/16.
 */
public class PrintOddEven {
    public static void main(String[] args) throws InterruptedException {

        Boolean integer = new Boolean(false);
        Thread thread1 = new Thread(new PrintNumber(integer, 1));
        Thread thread2 = new Thread(new PrintNumber(integer, 2));
        thread1.setName("Odd thread");
        thread2.setName("Two thread");

        thread1.start();
        thread2.start();


        thread1.join();
        thread2.join();

    }
}


class PrintNumber implements Runnable {

    private Boolean isEven;
    private int startNumber;

    public PrintNumber(Boolean aBoolean, int limit) {
        this.isEven = aBoolean;
        this.startNumber = limit;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " started ");
        while (startNumber <= 100) {
            synchronized (isEven) {
                if (startNumber % 2 != 0 && !isEven) {
                    System.out.println(startNumber);
                    startNumber += 2;
                    isEven = true;
                    isEven.notify();
                }

                if (startNumber % 2 == 0 && isEven) {
                    System.out.println("resched");
                    System.out.println(startNumber);
                    startNumber += 2;
                }
            }
        }
    }
}
