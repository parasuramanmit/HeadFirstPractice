package coding.interview;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * Created by parasuraman on 25/12/16.
 */
public class ConferenceMgmt {

    private static Map<String, Integer> eventNameToDuration = new HashMap<>();
    private static Map<String, Integer[]> sessionToEvents = new LinkedHashMap<>();


    static {
        sessionToEvents.put("morning1",new Integer[]{180,180});
        sessionToEvents.put("morning2",new Integer[]{180,180});
        sessionToEvents.put("evening1",new Integer[]{180,240});
        sessionToEvents.put("evening2",new Integer[]{180,240});
    }

    public static void main(String[] args) {
        File file = new File("/home/parasuraman/Desktop/input.txt");
        System.out.println(file.exists());
        String str = "";

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            while ((str = bufferedReader.readLine())!=null) {
                String[] split = str.split("\t");
                for (String string : split) {
                    if (string.endsWith("min")) {
                        string = string.replaceAll("\\D+","");
                        eventNameToDuration.put(str, Integer.valueOf(string));
                    }

                    if (string.endsWith("lightning")) {
                        eventNameToDuration.put(str, 5);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Integer> dummyMap = new HashMap<>(eventNameToDuration);

        Set<List<String>> morningSessionLists = compute1(new ArrayList<>(dummyMap.keySet()), new ArrayList<String>(), new HashSet<List<String>>(),180,180);
        if (!morningSessionLists.isEmpty()) {
            for (List<String> morningSessionList : morningSessionLists) {
                for (String s : morningSessionList) {
                    dummyMap.remove(s);
                }
                Set<List<String>> eveningSessionLists = compute1(new ArrayList<>(dummyMap.keySet()), new ArrayList<String>(), new HashSet<List<String>>(), 240, 180);

                if (!eveningSessionLists.isEmpty()) {
                    for (List<String> eveningSessionList : eveningSessionLists) {
                        for (String s : eveningSessionList) {
                            dummyMap.remove(s);
                        }
                        Set<List<String>> morSessionLists = compute1(new ArrayList<>(dummyMap.keySet()), new ArrayList<String>(), new HashSet<List<String>>(), 180, 180);
                        if (!morSessionLists.isEmpty()) {
                            for (List<String> morSessionList : morSessionLists) {
                                for (String s : morSessionList) {
                                    dummyMap.remove(s);
                                }
                                Set<List<String>> eveSessionLists = compute1(new ArrayList<>(dummyMap.keySet()), new ArrayList<String>(), new HashSet<List<String>>(), 240, 180);
                                if (!eveSessionLists.isEmpty()) {
                                    System.out.println(eveSessionLists);
                                }
                                for (String s : morSessionList) {
                                    dummyMap.put(s, eventNameToDuration.get(s));
                                }


                            }
                        }
                        for (String s : eveningSessionList) {
                            dummyMap.put(s, eventNameToDuration.get(s));
                        }

                    }
                }

                for (String s : morningSessionList) {
                    dummyMap.put(s, eventNameToDuration.get(s));
                }

            }
        }

        /*
        for (Map.Entry<String, Integer[]> entry : sessionToEvents.entrySet()) {
            String sessionName = entry.getKey();
            Integer[] value = entry.getValue();
            Set<List<String>> morningSessionLists = compute1(new ArrayList<>(eventNameToDuration.keySet()), new ArrayList<String>(), new HashSet<List<String>>(),value[0],value[1]);

            List<String> randomList = morningSessionLists.iterator().next();
            for (String random : randomList) {
                eventNameToDuration.remove(random);
            }
            System.out.println("For "+sessionName+" events are ");
            for (String s : randomList) {
                System.out.println(s);
            }

        }*/


    }


    public static Set<List<String>> compute1(List<String> inputs, List<String> seperatedList, Set<List<String>> result, int max, int min) {

        int sumofRemaining = getSumofRemaining1(seperatedList);

        if (sumofRemaining <= max && sumofRemaining >= min) {
            result.add(seperatedList);
        }

        if (sumofRemaining > max)
            return result;

        for (int i = 0; i< inputs.size() ;i++) {

            String string = inputs.get(i);

            List<String> seperatedList_rec = new ArrayList<>(seperatedList);

            seperatedList_rec.add(string);

            List<String> inputs_rec = new ArrayList<>();
            for (int j=i+1; j< inputs.size() ;j++) {
                inputs_rec.add(inputs.get(j));

            }
            compute1(inputs_rec, seperatedList_rec, result, max, min);
        }

        return result;
    }

    private static int getSumofRemaining1(List<String> seperatedList) {

        int sum = 0;
        for (String s : seperatedList) {
            Integer integer = eventNameToDuration.get(s);
            sum += integer;

        }
        return sum;
    }

/*
    public static void compute(List<Integer> inputs, List<Integer> seperatedList) {

        int sumofRemaining = getSumofRemaining(seperatedList);

        if (sumofRemaining == target) {
            result.add(seperatedList);
        }

        if (sumofRemaining > target)
            return;

        for (int i = 0; i< inputs.size() ;i++) {
            Integer integer = inputs.get(i);

            List<Integer> seperatedList_rec = new ArrayList<>(seperatedList);

            seperatedList_rec.add(integer);

            List<Integer> inputs_rec = new ArrayList<>();
            for (int j=i+1; j< inputs.size() ;j++) {
                inputs_rec.add(inputs.get(j));

            }
           //// System.out.println("inputs = [" + inputs_rec + "], seperatedList = [" + seperatedList_rec + "]");
            compute(inputs_rec, seperatedList_rec);

           // System.out.println("====================");

        }
    }

    private static int getSumofRemaining(Collection<Integer> inputs) {
        int sum = 0;
        for (int input : inputs) {
            sum += input;
        }
        return sum;
    }*/
}
