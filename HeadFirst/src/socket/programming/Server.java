package socket.programming;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedTransferQueue;

/**
 * Created by PARASURAMAN on 9/7/2016.
 */
public class Server {

    public void runServer() throws IOException, ClassNotFoundException, InterruptedException {

        ServerSocket serverSocket = new ServerSocket(4844);
        Socket socket = serverSocket.accept();

        ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());

        LinkedList<String> linkedList = (LinkedList<String>) is.readObject();
        String message = linkedList.pop();
        System.out.println("Message received from client");
        System.out.println(message);

        System.out.println("Sending message to client");
        String newMessage = new String("My new message");
        linkedList.add(newMessage);
        os.writeObject(linkedList);
        socket.close();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        new Server().runServer();
    }
}


