package socket.programming;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;

/**
 * Created by PARASURAMAN on 9/7/2016.
 */
public class Client {
    public static void main(String[] args) throws UnknownHostException,
            IOException, ClassNotFoundException {
        LinkedList<String> linkedList = new LinkedList<>();
        System.out.println("welcome client");
        Socket socket = new Socket("localhost", 4844);
        System.out.println("Client connected");
        ObjectOutputStream os = new ObjectOutputStream(socket.getOutputStream());
        System.out.println("Ok");
        String message = new String("Nesamani is learning socket programming");
        linkedList.add(message);
        os.writeObject(linkedList);
        System.out.println("Envoi des informations au serveur ...");

        ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
        LinkedList<String> linkedList1 = (LinkedList<String>) is.readObject();
        System.out.println("return Message is=" + linkedList1.pop());
        socket.close();
    }
}