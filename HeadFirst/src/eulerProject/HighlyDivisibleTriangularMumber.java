package src.eulerProject;

public class HighlyDivisibleTriangularMumber {

	public static void main(String[] args) throws Exception {
		long i = 1;
		long result = 0;
		boolean found = false;
		double startTime = System.currentTimeMillis();
		while (!found) {
			long triAngleNumber = summer(i);
			long receivedCount = getDivisors(triAngleNumber);
			if (receivedCount >= 500) {
				result = triAngleNumber;
				found = true;
				 System.out.println("For number " + triAngleNumber + " count is "+ receivedCount);
			}
			i++;
		}
		double endTime = System.currentTimeMillis();
	    System.out.println("Took "+((endTime - startTime) / 1000)+" seconds"); 
		System.out.println(result);
	}

	private static long getDivisors(long triAngleNumber) {
		long count = 0;
		for (long i = 1; i*i <= triAngleNumber; i++) {
			if (triAngleNumber % i == 0) {
				count+=2;
			}
		}
		//Returning count
		return count;
	}

	private static long summer(long i) {
		long sum = 0;
		for (long j = 1; j <= i; j++) {
			sum += j;
		}
		return sum;
	}
}
