package eulerProject;

public class PythagoreanTriplet {
	public static void main(String[] args) {

		for (int i = 1; i < 1000; i++) {
			for (int j = i + 1; j < 1000; j++) {
				for (int k = j + 1; k < 1000; k++) {
					
					double powI = Math.pow(i, 2);
					double powj = Math.pow(j, 2);
					double powk = Math.pow(k, 2);
					
					if((i+j+k) == 1000 && ((powI+powj) == powk)) {
						System.out.println(i+" "+j+" "+k+" "+"Eureka");
						System.out.println(i*j*k);
					}
				}
			}
		}
	}
}
