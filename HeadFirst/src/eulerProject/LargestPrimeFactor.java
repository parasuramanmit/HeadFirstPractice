package eulerProject;

public class LargestPrimeFactor {
	public static void main(String[] args) {
		
		for (long i = 1L; i < 600851475143L ; i++) {
			boolean flag = false;
			for (long j = 2L; j < i; j++) {
				if (i % j == 0) {
					flag = true;
					break;
				}
			}
			if (flag == false) {
				//System.out.println(i + " is a prime number");
				if (600851475143L % i == 0) {
					System.out.println("Perfect divisor "+i);
				}
			}
		}
	}
}
