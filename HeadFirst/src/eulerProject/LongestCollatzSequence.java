public class LongestCollatzSequence {

	public static void main(String[] args) throws Exception {
		long largestCount = 0;
		long largestNumber = 0;
		long startTime = System.currentTimeMillis();
		for (long number = 2; number <= 1000000; number++) {
			long tempNumber = number;
			long count = 0;
			while (tempNumber != 1) {
				if (tempNumber % 2 == 0) {
					tempNumber = tempNumber / 2;
				} else {
					tempNumber = 3 * tempNumber + 1;
				}
				count++;
			}
			if (count > largestCount) {
				largestCount = count;
				largestNumber = number;
			}
		}
		System.out.println("largestNumber is " + largestNumber);
		System.out.println("Time take "+(System.currentTimeMillis()-startTime));
	}
}
