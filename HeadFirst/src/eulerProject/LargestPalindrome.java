package eulerProject;

public class LargestPalindrome {

	public static void main(String[] args) {

		int biggestPalin = 0;
		
		for (int k = 100; k <= 999; k++) {
			for (int l = 100; l <= 999; l++) {
				boolean palindrome = checkifPalindrome(k * l);
				if (palindrome) {
					
					if(biggestPalin < (k*l)){
						biggestPalin = (k*l);	
					}
				}
			}
		}
		System.out.println(biggestPalin);
	}

	private static boolean checkifPalindrome(int numberToCheck) {
		int j = numberToCheck;
		int temp = 0;
		while (j > 0) {
			temp *= 10;
			temp += j % 10;
			j /= 10;
		}
		if (numberToCheck == temp) {
			return true;
		}

		return false;
	}
}
