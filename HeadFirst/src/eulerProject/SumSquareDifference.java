package eulerProject;

public class SumSquareDifference {
	public static void main(String[] args) {
		double sumOfSquare = 0;
		double sumOfNumbers = 0;
		for (int i = 1; i <= 100; i++) {
			double pow = Math.pow(i, 2);
			sumOfSquare+=pow;
			sumOfNumbers += i;
		}
		System.out.println(sumOfSquare);
		System.out.println(Math.pow(sumOfNumbers, 2));
		double result = Math.pow(sumOfNumbers, 2) - sumOfSquare;
		System.out.println((long)result);
	}
}
