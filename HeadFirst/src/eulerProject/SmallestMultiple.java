package eulerProject;

public class SmallestMultiple {
	public static void main(String[] args) {
		boolean found = false;
		int number = 11;
		while (!found) {
			found = true;
			for (int i = 1; i <= 20; i++) {
				if (number % i != 0) {
					number++;
					found = false;
					break;
				}
			}
			if (found == true) {
				System.out.println("Number is " + number);
			}

		}
	}
}
