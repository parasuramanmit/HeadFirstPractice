package solutions.easy.hacker.earth;

import com.sun.java.swing.plaf.windows.WindowsInternalFrameTitlePane;

import java.util.Scanner;

/**
 * Created by parasuraman on 10/10/16.
 */
public class Triplets {
    public static void main(String[] args) {
        int[] scores = new int[6];

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 6; i++) {
            scores[i] = scanner.nextInt();
        }

        int aliceScore = 0;
        int bobScore = 0;

        for (int i = 0; i < 3; i++) {
            if (scores[i] > scores[i+3])
                aliceScore++;
            if (scores[i] < scores[i+3])
                bobScore++;
        }


        System.out.println(aliceScore+" "+bobScore);
    }
}
