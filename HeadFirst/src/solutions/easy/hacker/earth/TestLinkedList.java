package solutions.easy.hacker.earth;

/**
 * Created by parasuraman on 5/10/16.
 */
public class TestLinkedList {
    public static void main(String[] args) {
        Node node = new Node(10);
        node.insert(9);

        System.out.println(node);
    }

    public void print(Node head) {

    }
}


class Node {
    private int data;
    private Node next;
    private Node head;

    public Node(int data) {
        this.data = data;
        head = this;
    }

    public void insert(int data) {
        Node newNode = new Node(data);
        this.next = newNode;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        while (head != null) {
            stringBuilder.append(data + " ");
            head = next;
        }
        return stringBuilder.toString();
    }
}
