package solutions.easy.hacker.earth;

import java.util.Scanner;

/**
 * Created by parasuraman on 28/9/16.
 */
public class JustArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();

        int[] array = new int[count];

        for (int i=0;i<count;i++) {
            array[i] = scanner.nextInt();
        }

        for (int i=array.length-1;i>=0;i--) {
            System.out.println(array[i]);
        }
    }
}
