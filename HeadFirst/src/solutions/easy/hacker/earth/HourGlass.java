package solutions.easy.hacker.earth;

import java.util.Scanner;

/**
 * Created by parasuraman on 28/9/16.
 */
public class HourGlass {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[][] array = new int[6][6];

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                array[i][j] = scanner.nextInt();
            }
        }


        int sum = Integer.MIN_VALUE;

        for (int row = 0; row < 4; row++) {
            for (int column = 0; column < 4; column++) {
                int i = array[row][column] + array[row][column + 1] + array[row][column + 2]
                        +array[row+1][column+1]+array[row+2][column]+array[row+2][column+1]+array[row+2][column+2];
                if (i > sum) {
                    sum = i;
                }
            }
        }

        System.out.println(sum);

    }
}
