package solutions.easy.hacker.earth;

import java.util.Scanner;

/**
 * Created by parasuraman on 10/10/16.
 */
public class ArrayRotaion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int rotations = scanner.nextInt();
        int queryCount = scanner.nextInt();
        int[] numbers = new int[count];
        int[] queries = new int[queryCount];
        for (int i = 0; i < count; i++) {
            numbers[i] = scanner.nextInt();
        }
        for (int i=0;i<queryCount;i++) {
            queries[i] = scanner.nextInt();
        }

        for (int j = 0; j < rotations; j++) {
            for (int i = numbers.length - 1; i > 0; i--) {
                int tmp = numbers[i];
                numbers[i] = numbers[i - 1];
                numbers[i - 1] = tmp;
            }
        }

        for (int i=0;i<queryCount;i++) {
            System.out.println(numbers[queries[i]]);
        }

    }
}
