package solutions.easy.hacker.earth;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by parasuraman on 5/10/16.
 */
public class SparseArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int stringCount = scanner.nextInt();

        String[] strArray = new String[stringCount];

        for (int i = 0; i < stringCount; i++) {
            strArray[i] = scanner.next();
        }

        int queriesCount = scanner.nextInt();
        String[] queryArray = new String[queriesCount];
        for (int i = 0; i < queriesCount; i++) {
            queryArray[i] = scanner.next();
        }

        for (String s : queryArray) {

            int count =0;
            for (String s1 : strArray) {
                if (s1.equals(s)) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }
}
