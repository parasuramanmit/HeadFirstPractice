package solutions.easy.hacker.earth;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by parasuraman on 11/10/16.
 */
public class DivisibleSumPairs {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int divisor = scanner.nextInt();
        int[] numbers = new int[count];
        for (int i =0;i<count;i++) {
            numbers[i] = scanner.nextInt();
        }
//        Arrays.sort(numbers);
  //      System.out.println(Arrays.toString(numbers));

        int result = 0;
        for (int i = 0; i < numbers.length ; i++) {
            for (int j = i + 1; j < numbers.length ; j++) {
                if ((numbers[i] + numbers[j]) % divisor == 0) {
                   // System.out.println(numbers[i] +" "+ numbers[j]);
                    result++;
                }
            }
        }

        System.out.println(result);
    }
}
