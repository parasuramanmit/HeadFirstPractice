package solutions.easy.hacker.earth;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by parasuraman on 10/10/16.
 */
public class DegenerateTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int[] numbers = new int[count];
        for (int i=0;i<count;i++) {
            numbers[i] = scanner.nextInt();
        }
        Arrays.sort(numbers);
        //System.out.println(Arrays.toString(numbers));
        int x,y,z;
        for (x = count - 3, y = count - 2, z = count - 1; numbers[x] + numbers[y] >= numbers[z]; x--, y--, z--) {
            //System.out.println(numbers[x]+" "+ numbers[y]+" "+ ">="+ numbers[z]);
            if (x == 0) {
                System.out.println(-1);
                return;
            }
        }
        System.out.println(numbers[x]+" "+numbers[y]+" "+numbers[z]);
    }
}
