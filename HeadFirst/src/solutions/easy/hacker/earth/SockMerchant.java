package solutions.easy.hacker.earth;

import java.util.Scanner;

/**
 * Created by parasuraman on 12/10/16.
 */
public class SockMerchant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        boolean[] boolArray = new boolean[101];
        int result = 0;
        for (int i = 0; i < count; i++) {
            int number = scanner.nextInt();
            if (boolArray[number]) {
                result++;
                boolArray[number] = false;
            } else {
                boolArray[number] = true;
            }
        }
        System.out.println(result);

    }
}
