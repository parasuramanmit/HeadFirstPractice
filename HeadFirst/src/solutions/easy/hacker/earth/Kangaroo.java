package solutions.easy.hacker.earth;

import java.util.Scanner;

/**
 * Created by parasuraman on 10/10/16.
 */
public class Kangaroo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x1 = in.nextInt();
        int v1 = in.nextInt();
        int x2 = in.nextInt();
        int v2 = in.nextInt();

        for (int i = 1; i <= 10000; i++) {
            if ((x1 + (i * v1)) == (x2 + (i * v2))) {
                System.out.println("YES");
                return;
            }
        }

        System.out.println("NO");

    }
}
