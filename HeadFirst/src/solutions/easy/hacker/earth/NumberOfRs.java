package solutions.easy.hacker.earth;

public class NumberOfRs {
	public static void main(String[] args) {
		int array[] = { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
		doKadane(array);
	}

	private static void doKadane(int[] array) {
		int max_curr = 0;
		int max_sofar = 0;
		for (int i = 0; i < array.length; i++) {
			max_curr = max(0, array[i] + max_curr);
			max_sofar = max(max_curr, max_sofar);
		}
		System.out.println(max_sofar);
	}

	private static int max(int i, int j) {
		return i > j ? i : j;
	}

}
