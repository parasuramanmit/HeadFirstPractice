package practice;

/**
 * Created by parasuraman on 6/12/16.
 */



public interface  Human {
    int getAge();
}



class Student implements Human {

    public int getAge() {
        int age = 98 * 3457;
        return age;
    }
}

class Teacher implements Human {
    public int getAge() {
        return 0;
    }
}


class Test {
    public static void main(String[] args) {

        google(new Teacher());
        google(new Student());

    }
    public static void google(Human human) {
        int age = human.getAge();
        System.out.println(age);
    }
}