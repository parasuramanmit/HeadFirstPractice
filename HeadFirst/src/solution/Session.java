package solution;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Parasuraman R on 12/27/16.
 */
public class Session {

    private String starTime;
    private String endTime;
    private Map<String,Integer> eventNameToDuration;
    private List<String> results = new LinkedList<>();
    private SessionType sessionType;

    public Session(String starTime, String endTime, SessionType sessionType) {
        this.starTime = starTime;
        this.endTime = endTime;
        this.sessionType = sessionType;
    }

    public SessionType getSessionType() {
        return sessionType;
    }

    public void printEventSchedule() {
        for (String result : results) {
            System.out.println(result);
        }
    }

    public void scheduleEvents(Map<String, Integer> map) {
        this.eventNameToDuration = map;
    }

    public void organizeEVents()  {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar cal = Calendar.getInstance();

        try {
            cal.setTime(df.parse(starTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (Map.Entry<String, Integer> entry : eventNameToDuration.entrySet()) {
            String eventName = entry.getKey();
            Integer duration = entry.getValue();
            String result = cal.getTime() + " " + eventName;
            cal.add(Calendar.MINUTE, duration);
            results.add(result);
        }
        endTime = cal.getTime().toString();
    }

    public void addLunchTime(String lunchTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(lunchTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        results.add(cal.getTime() + " "+"Lunch");
    }

    public void setSpecialEvent(String specialEvent) {
        results.add(endTime+ "  "+specialEvent);
    }

    public String getSessoinName() {
        return sessionType.toString();
    }
}