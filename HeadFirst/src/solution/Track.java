package solution;

import java.text.ParseException;
import java.util.*;

/**
 * Created by Parasuraman R on 12/27/16.
 */
public class Track {
    private List<Session> sessions = new LinkedList<>();
    private String lunchTime = "2016-12-31 12:00";
    private String specialEvent = "Networking event";
    private  Map<String, Integer> eventToDuration = new HashMap<>();
    private String name ;

    public Track(String name, Map<String, Integer> eventToDuration) {
        this.eventToDuration = eventToDuration;
        this.name = name;
        createSessions();
    }

    private void createSessions() {
        Session morningSession = new Session("2016-12-31 09:00", "2016-12-31 12:00", SessionType.MORNING);
        Session eveningSession = new Session("2016-12-31 13:00", "2016-12-31 17:00", SessionType.EVENING);
        sessions.add(morningSession);
        sessions.add(eveningSession);
    }

    public Map<String, Integer> scheduleMorningSession(Map<String, Integer> map) {
        for (Session session : sessions) {
            if (session.getSessionType() == SessionType.MORNING) {
                Map<String, Integer> calculate = calculate(map, 180, 180);
                session.scheduleEvents(calculate);
                for (String s : calculate.keySet()) {
                    map.remove(s);
                }
                session.organizeEVents();
                session.addLunchTime(lunchTime);
            }
        }
        return map;
    }

    public Map<String, Integer> scheduleEveningSession(Map<String, Integer> map) {
        for (Session session : sessions) {
            if (session.getSessionType() == SessionType.EVENING) {
                if (getSum(new ArrayList<>(map.keySet())) <= 240) {
                    session.scheduleEvents(map);
                } else {
                    Map<String, Integer> calculate = calculate(map, 210, 240);
                    session.scheduleEvents(calculate);
                    for (String s : calculate.keySet()) {
                        map.remove(s);
                    }
                }

                session.organizeEVents();
                session.setSpecialEvent(specialEvent);
            }
        }
        return map;
    }

    public  Map<String, Integer> calculate(Map<String, Integer> map,int min, int max) {
        return calculate(new ArrayList<>(map.keySet()), min, max);
    }

    public  Map<String, Integer> calculate(List<String> input, int min, int max) {

        int n = input.size();

        for (int i = 0; i < (1 << n); i++) {

            List<String> list = new ArrayList<>();

            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) > 0) {
                    list.add(input.get(j));
                }
            }

            int sum = getSum(list);
            if (sum >= min && sum <= max) {
                Map<String, Integer> map = new LinkedHashMap<>();
                for (String s : list) {
                    map.put(s, eventToDuration.get(s));
                }
                return map;
            }
        }
        return Collections.emptyMap();
    }

    public  int getSum(List<String> list) {
        int sum = 0;
        for (String string : list) {
            sum = sum + eventToDuration.get(string);
        }
        return sum;
    }

    public void printSchedule() throws ParseException {
        for (Session session : sessions) {
            System.out.println(name+" "+session.getSessoinName());

            session.printEventSchedule();
            System.out.println("================");
        }
    }

}


