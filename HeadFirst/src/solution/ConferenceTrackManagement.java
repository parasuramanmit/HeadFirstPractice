package solution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.ParseException;
import java.util.*;

/**
 * Created by Parasuraman R on 12/26/16.
 */
public class ConferenceTrackManagement {

    private static int allowedTimeMinutes = 840;

    public static void main(String[] args) throws ParseException {
        String filePath = "/home/input.txt";

        Map<String, Integer> eventsToDuration = getEventToDuration(filePath);

        Collection<Integer> values = eventsToDuration.values();
        int totalDuration = 0;
        for (Integer value : values) {
            totalDuration += value;
        }

        if (totalDuration > allowedTimeMinutes) {
            System.out.println("Events can't be accomodated in the given interval");
            return;
        }

        Map<String, Integer> eventsToDur = new HashMap<>(eventsToDuration);

        List<Track> tracks = getTracks(eventsToDuration);
        for (Track track : tracks) {
            track.scheduleMorningSession(eventsToDur);
        }

        for (Track track : tracks) {
            track.scheduleEveningSession(eventsToDur);
        }

        for (Track track : tracks) {
            track.printSchedule();
        }
    }

    private static List<Track> getTracks(Map<String, Integer> eventsToDuration) {
        Track track1 = new Track("Track1",eventsToDuration);
        Track track2 = new Track("Track2",eventsToDuration);

        List<Track> tracks = new ArrayList<>();
        tracks.add(track1);
        tracks.add(track2);

        return tracks;
    }


    public static Map<String,Integer> getEventToDuration(String filePath) {
        File file = new File(filePath);
        Map<String, Integer> eventToDuration = new HashMap<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String str;
            while ((str = bufferedReader.readLine()) != null) {
                String[] split = str.split(" ");

                String last = split[split.length-1];

                if (last.endsWith("min")) {
                    last = last.replaceAll("\\D+","");
                    eventToDuration.put(str, Integer.valueOf(last));
                }

                if (str.endsWith("lightning")) {
                    eventToDuration.put(str, 5);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return eventToDuration;
    }
}