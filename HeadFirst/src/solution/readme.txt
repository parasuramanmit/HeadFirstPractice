
1.Place the following files in a package named 'solution'
    1.ConferenceTrackManagement.java
    2.Session.java
    3.SessionType.java
    4.Track.java

2.Run ConferenceTrackManagement.java to execute the program.

3.The program assumes the following.
    1. The program will be executed in a Unix based Operating system. Based on that the directory path has '/' separator.
        For Windows it should be replaced with '\'

    2. Place the file under the /home/ directory and name it as 'input.txt'.

